Cow Information
========================
Project developed for the selection of the company [**TAG Interativa**](http://mmmm.mu/).

About Project
========================
Project using the Symfony2 framework. The project will receive information about a cow and will send to database using an [**API**](http://recrutamento.taginterativa.com.br/api/v1/).

How to use this project?
========================
1. Clone this repository and update the **composer**.
2. Duplicate the file: **app/config/parameters.sample.yml** to **app/config/parameters.yml**
3. Set all permission to **app/cache** and to **app/logs**

Conclusion
========================
Project used to about set cow information and return a cow better (cust-benefit). Only using the API received.

Developed by
========================
[**Leonardo Rifeli**](mailto:leonardorifeli@gmail.com).
[**leonardorifeli.com**](https://leonardorifeli.com/).

License
========================
[MIT License](http://leonardorifeli.mit-license.org/) © Leonardo Rifeli
