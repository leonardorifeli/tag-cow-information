<?php

namespace CowInformation\Bundle\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ListController extends Controller
{
    private $cowService;

    private function getCowService()
    {
        if(!$this->cowService) {
            $this->cowService = $this->get('cow.service');
        }

        return $this->cowService;
    }

    public function indexAction()
    {
        $results = $this->getCowService()->listAllOrderedByFavorite();

        return $this->render('MainBundle:List:index.html.twig', array(
            'results' => $results
        ));
    }
}
