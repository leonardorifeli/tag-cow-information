<?php

namespace CowInformation\Bundle\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CowInformation\Bundle\BusinessBundle\Business\Enum\HttpCodeStatus;

class DeleteController extends Controller
{
    private $cowService;

    private function getCowService()
    {
        if(!$this->cowService) {
            $this->cowService = $this->get('cow.service');
        }

        return $this->cowService;
    }

    public function deleteAction($id)
    {
        $result = $this->getCowService()->find($id);

        if($result){
            try{
                $delete = $this->getCowService()->delete($id);

                if($delete){
                    return $this->render('MainBundle:Delete:index.html.twig', array(
                        'message' => '',
                        'status' => HttpCodeStatus::ALL_SUCCESS,
                    ));
                }
            } catch (\Exception $e){
                $message = $e->getMessage();

                return $this->render('MainBundle:Delete:index.html.twig', array(
                    'message' => $message,
                    'status' => HttpCodeStatus::NO_SUCCESS,
                ));
            }
        }

        return $this->redirect($this->generateUrl('list_main', array(
            'delete' => HttpCodeStatus::NO_SUCCESS
        )));
    }
}
