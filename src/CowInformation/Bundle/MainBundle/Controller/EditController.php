<?php

namespace CowInformation\Bundle\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use CowInformation\Bundle\BusinessBundle\Business\Model\Cow;
use CowInformation\Bundle\BusinessBundle\Business\Enum\Status;
use CowInformation\Bundle\MainBundle\Form\CowType;

class EditController extends Controller
{
    private $cowService;

    private function getCowService()
    {
        if(!$this->cowService) {
            $this->cowService = $this->get('cow.service');
        }

        return $this->cowService;
    }

    public function indexAction($id, $form = null, $message = null)
    {
        $result = $this->getCowService()->find($id);

        if(!$result){
            return $this->redirect($this->generateUrl('list_main'));
        }

        if(!$form){
            $form = $this->createEditForm($result);
        }

        return $this->render('MainBundle:Edit:index.html.twig', array(
            'result' => $result,
            'message' => $message,
            'form' => $form->createView()
        ));
    }

    public function updateAction(Request $request, $id)
    {
        $model = $this->getCowService()->find($id);

        if(!$model){
            return $this->redirect($this->generateUrl('list_main'));
        }

        $form = $this->createEditForm($model);
        $form->handleRequest($request);

        if ($form->isValid()) {
            try{
                $edit = $this->getCowService()->edit($model);

                return $this->redirect($this->generateUrl('cow_edit_main', array(
                    'success' => Status::SUCCESS,
                    'id' => $model->getId(),
                )));
            }catch(\Exception $e){
                $message = $e->getMessage();

                return $this->indexAction($model->getId(), $form, $message);
            }
        }
    }

    private function createEditForm($model)
    {
        $form = $this->createForm(new CowType(), $model, array(
            'action' => $this->generateUrl('cow_update_main', array('id' => $model->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Editar',
            'attr' => array(
                'class'=>'btn btn-primary'
            ))
        );

        return $form;
    }
}
