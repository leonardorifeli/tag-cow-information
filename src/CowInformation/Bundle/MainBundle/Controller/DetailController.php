<?php

namespace CowInformation\Bundle\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DetailController extends Controller
{
    private $cowService;

    private function getCowService()
    {
        if(!$this->cowService) {
            $this->cowService = $this->get('cow.service');
        }

        return $this->cowService;
    }

    public function indexAction($id)
    {
        $result = $this->getCowService()->find($id);

        if(!$result){
            return $this->redirect($this->generateUrl('list_main'));
        }

        return $this->render('MainBundle:Detail:index.html.twig', array(
            'result' => $result
        ));
    }
}
