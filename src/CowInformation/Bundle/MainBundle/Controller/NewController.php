<?php

namespace CowInformation\Bundle\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use CowInformation\Bundle\BusinessBundle\Business\Model\Cow;
use CowInformation\Bundle\BusinessBundle\Business\Enum\Status;
use CowInformation\Bundle\MainBundle\Form\CowType;

class NewController extends Controller
{

    private $cowService;

    private function getCowService()
    {
        if(!$this->cowService) {
            $this->cowService = $this->get('cow.service');
        }

        return $this->cowService;
    }

    public function newAction($form = null, $message = null)
    {
        if(!$form){
            $model = new Cow();
            $form = $this->createCreateForm($model);
        }

        return $this->render('MainBundle:New:index.html.twig', array(
            'form' => $form->createView(),
            'message' => $message
        ));
    }

    public function createAction(Request $request)
    {
        $model = new Cow();
        $form = $this->createCreateForm($model);
        $form->handleRequest($request);

        if ($form->isValid()) {
            try{
                $new = $this->getCowService()->create($model);

                return $this->redirect($this->generateUrl('cow_new_main', array(
                    'success' => Status::SUCCESS,
                )));
            }catch(\Exception $e){
                $message = $e->getMessage();

                return $this->newAction($form, $message);
            }
        }
    }

    private function createCreateForm(Cow $model)
    {
        $form = $this->createForm(new CowType(), $model, array(
            'action' => $this->generateUrl('cow_create_main'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Cadastrar',
            'attr' => array(
                'class'=>'btn btn-primary'
            ))
        );

        return $form;
    }
}
