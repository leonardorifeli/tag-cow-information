<?php

namespace CowInformation\Bundle\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CowType extends AbstractType
{

    /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('weight', 'integer', array(
            'required' => true,
        ))
        ->add('age', 'integer', array(
            'required' => true,
        ))
        ->add('price', 'number', array(
            'required' => true,
            'grouping' => true,
        ));
    }

    /**
    * @param OptionsResolverInterface $resolver
    */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CowInformation\Bundle\BusinessBundle\Business\Model\Cow'
        ));
    }

    /**
    * @return string
    */
    public function getName()
    {
        return 'cow_type';
    }
}
