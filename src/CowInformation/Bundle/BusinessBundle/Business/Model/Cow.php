<?php

namespace CowInformation\Bundle\BusinessBundle\Business\Model;

use CowInformation\Bundle\BusinessBundle\Business\Enum\Pasture;

class Cow{

    private $id;
    private $weight;
    private $age;
    private $price;
    private $totalPastureInKg;
    private $totalPastureInReal;

    /**
    * Get the value of Weight
    *
    * @return mixed
    */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
    * Set the value of Weight
    *
    * @param mixed weight
    *
    * @return self
    */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
    * Get the value of Age
    *
    * @return mixed
    */
    public function getAge()
    {
        return $this->age;
    }

    /**
    * Set the value of Age
    *
    * @param mixed age
    *
    * @return self
    */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
    * Get the value of Price
    *
    * @return mixed
    */
    public function getPrice()
    {
        return $this->price;
    }

    /**
    * Set the value of Price
    *
    * @param mixed price
    *
    * @return self
    */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }


    /**
    * Get the value of Id
    *
    * @return mixed
    */
    public function getId()
    {
        return $this->id;
    }

    /**
    * Set the value of Id
    *
    * @param mixed id
    *
    * @return self
    */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getTotalPastureInReal()
    {
        $this->totalPastureInReal = ($this->getTotalPastureInKg() * Pasture::PRICE_PER_KG);

        return $this->totalPastureInReal;
    }

    public function getTotalPastureInKg()
    {
        $this->totalPastureInKg = ($this->weight * Pasture::WEIGHT_PER_HUNDRED)/100;

        return $this->totalPastureInKg;
    }

}
