<?php

namespace CowInformation\Bundle\BusinessBundle\Business\Enum;

class Status
{
    const SUCCESS = 1;
    const ERROR = 0;
}
