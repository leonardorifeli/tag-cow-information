<?php

namespace CowInformation\Bundle\BusinessBundle\Business\Enum;

class HttpCodeStatus
{
    const CREATED = 201;
    const ALL_SUCCESS = 1;
    const NO_SUCCESS = 0;
    const SUCCESS = 200;
}
