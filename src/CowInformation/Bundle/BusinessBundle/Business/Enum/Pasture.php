<?php

namespace CowInformation\Bundle\BusinessBundle\Business\Enum;

class Pasture
{
    const WEIGHT_PER_HUNDRED = 3;
    const PRICE_PER_KG = 0.20;
}
