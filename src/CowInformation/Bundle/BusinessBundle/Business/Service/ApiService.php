<?php

namespace CowInformation\Bundle\BusinessBundle\Business\Service;

use CowInformation\Bundle\BusinessBundle\Business\Enum\HttpCodeStatus;

class ApiService
{
    private $buzzService;
    private $serviceContainer;

    public function __construct($buzzService, $serviceContainer)
    {
        $this->buzzService = $buzzService;
        $this->serviceContainer = $serviceContainer;
    }

    private function getApiHeader()
    {
        if(!$this->serviceContainer->getParameter('api')['token']){
            throw new \Exception("Please, inform the API TAG Interativa in the parameters file.");
        }

        if(!$this->serviceContainer->getParameter('api')['url']){
            throw new \Exception("Please, inform the URL of API TAG Interativa in the parameters file.");
        }

        $headers = array(
            'Content-Type' => 'application/json',
            'access-token' => $this->serviceContainer->getParameter('api')['token']
        );

        return $headers;
    }

    public function find($id)
    {
        if($id){
            $headers = $this->getApiHeader();
            $response = $this->buzzService->get("{$this->serviceContainer->getParameter('api')['url']}/{$id}", $headers);

            if($response->getStatusCode() == HttpCodeStatus::SUCCESS){
                return json_decode($response->getContent());
            }else{
                return false;
            }
        }

        return false;
    }

    public function delete($id)
    {
        if(!$id){
            throw new \Exception("Please, inform the register id.");
        }

        $headers = $this->getApiHeader();
        $response = $this->buzzService->delete("{$this->serviceContainer->getParameter('api')['url']}/{$id}", $headers);

        if($response->getStatusCode() == HttpCodeStatus::SUCCESS){
            return true;
        }else{
            return false;
        }
    }

    public function edit($model)
    {
        $headers = $this->getApiHeader();

        $data['weight'] = $model->getWeight();
        $data['age'] = $model->getAge();
        $data['price'] = $model->getPrice();
        $data = json_encode($data);

        $response = $this->buzzService->put("{$this->serviceContainer->getParameter('api')['url']}/{$model->getId()}", $headers, $data);

        if($response->getStatusCode() == HttpCodeStatus::SUCCESS){
            return json_decode($response->getContent());
        }else{
            return false;
        }
    }

    public function create($model)
    {
        $headers = $this->getApiHeader();

        $data['weight'] = $model->getWeight();
        $data['age'] = $model->getAge();
        $data['price'] = $model->getPrice();

        $response = $this->buzzService->post($this->serviceContainer->getParameter('api')['url'], $headers, $data);

        if($response->getStatusCode() == HttpCodeStatus::CREATED){
            return json_decode($response->getContent());
        }else{
            return false;
        }
    }

    public function listAllOrderedByFavorite()
    {
        $headers = $this->getApiHeader();

        $response = $this->buzzService->get("{$this->serviceContainer->getParameter('api')['url']}?order_by[price]=asc&order_by[weight]=asc", $headers);
        $response = $response->getContent();
        $response = json_decode($response);

        return $response;
    }

    public function listAllOrderedById()
    {
        $headers = $this->getApiHeader();

        $response = $this->buzzService->get("{$this->serviceContainer->getParameter('api')['url']}?order_by[id]=DESC", $headers);
        $response = $response->getContent();
        $response = json_decode($response);

        return $response;
    }

}
