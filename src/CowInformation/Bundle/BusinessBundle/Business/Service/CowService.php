<?php

namespace CowInformation\Bundle\BusinessBundle\Business\Service;

use CowInformation\Bundle\BusinessBundle\Business\Model\Cow;

class CowService
{

    private $apiService;

    public function __construct($apiService)
    {
        $this->apiService = $apiService;
    }

    public function listAllOrderedById()
    {
        $results = $this->apiService->listAllOrderedById();

        $arrayResult = array();
        if($results){
            foreach($results as $key => $result){
                $arrayResult[] = $this->getCowObject($result);
            }
        }

        return $arrayResult;
    }

    public function listAllOrderedByFavorite()
    {
        $results = $this->apiService->listAllOrderedByFavorite();

        $arrayResult = array();
        if($results){
            foreach($results as $key => $result){
                $arrayResult[] = $this->getCowObject($result);
            }
        }

        return $arrayResult;
    }

    public function find($id)
    {
        $result = $this->apiService->find($id);
        $result = $this->getCowObject($result);

        return $result;
    }

    public function edit($model)
    {
        if($model->getAge() > 20){
            throw new \Exception("A idade não podera exceder os 20 (vinte) anos.");
        }

        $api = $this->apiService->edit($model);

        if($api->id){
            $api = $this->getCowObject($api);
        }

        return $api;
    }


    public function create($model)
    {
        if($model->getAge() > 20){
            throw new \Exception("A idade não podera exceder os 20 (vinte) anos.");
        }

        $apiCreate = $this->apiService->create($model);

        if($apiCreate->id){
            $apiCreate = $this->getCowObject($apiCreate);
        }

        return $apiCreate;
    }

    public function delete($id)
    {
        if(!$id){
            throw new \Exception("Please, inform the register id.");
        }

        $result = $this->find($id);

        if(!$result){
            throw new \Exception("Register not found.");
        }

        $delete = $this->apiService->delete($id);

        return $delete;
    }

    private function getCowObject($model)
    {
        $cow = new Cow();
        $cow->setId($model->id);
        $cow->setWeight($model->weight);
        $cow->setAge($model->age);
        $cow->setPrice($model->price);

        return $cow;
    }

}
